var billingApp = angular.module('billingApp', [
  'ngRoute',
  'ngAnimate',
  'billingControllers',
  'billingServices',
  'billingFilters',
  'billingDirectives',
  'ui.bootstrap',
  'ui.bootstrap.datetimepicker'
]);

billingApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/partners', {
        templateUrl: 'views/partners/partners-list.html',
        controller: 'PartnerListCtrl'
      }).
      when('/partners/:partnerId', {
        templateUrl: 'views/partners/partner-profile.html',
        controller: 'PartnerProfileCtrl'
      }).
      when('/partners/:partnerId/edit', {
        templateUrl: 'views/partners/edit_partner.html',
        controller: 'EditPartnerCtrl'
      }).
      when('/partners/:partnerId/receipts/new', {
        templateUrl: 'views/partners/receipts/new/new.html',
        controller: 'PartnerNewReceiptCtrl'
      }).
      when('/receipts/:receiptId', {
        templateUrl: 'views/receipts/profile.html',
        controller: 'ReceiptProfileCtrl'
      }).
      when('/receipts', {
        templateUrl: 'views/receipts/list.html',
        controller: 'ReceiptListCtrl'
      }).
      when('/partner_categories', {
        templateUrl: 'views/partner_categories/partner-categories-list.html',
        controller: 'PartnerCategoryListCtrl'
      }).
      when('/new_partner', {
        templateUrl: 'views/partners/edit_partner.html',
        controller: 'NewPartnerCtrl'
      }).
      when('/partner_categories/:partnerCategoryId', {
        templateUrl: 'views/partner_categories/partner-category-profile.html',
        controller: 'PartnerCategoryProfileCtrl'
      }).
      when('/partner_categories', {
        templateUrl: 'views/partner_categories/partner-categories-list.html',
        controller: 'PartnerCategoryListCtrl'
      }).
      when('/login', {
        templateUrl: 'views/auth/login.html',
        controller: 'LoginCtrl'
      }).
      when('/new_partner_category', {
        templateUrl: 'views/partner_categories/edit.html',
        controller: 'NewPartnerCategoryCtrl'
      }).
      when('/partner_categories/:partnerCategoryId/edit', {
        templateUrl: 'views/partner_categories/edit.html',
        controller: 'EditPartnerCategoryCtrl'
      }).
      when('/users', {
        templateUrl: 'views/users/list.html',
        controller: 'UsersListCtrl'
      }).
      when('/users/new', {
        templateUrl: 'views/users/edit.html',
        controller: 'NewUserCtrl'
      }).
      when('/users/:userId/edit', {
        templateUrl: 'views/users/edit.html',
        controller: 'EditUserCtrl'
      }).
      when('/seminaries', {
        templateUrl: 'views/seminaries/list.html',
        controller: 'SeminariesListCtrl'
      }).
      when('/seminaries/:seminaryId', {
        templateUrl: 'views/seminaries/profile.html',
        controller: 'SeminaryProfileCtrl'
      }).
      when('/new_seminary', {
        templateUrl: 'views/seminaries/edit.html',
        controller: 'NewSeminaryCtrl'
      }).
      when('/seminaries/:seminaryId/edit', {
        templateUrl: 'views/seminaries/edit.html',
        controller: 'EditSeminaryCtrl'
      }).
      when('/payments', {
        templateUrl: 'views/payments_reviews/index.html',
        controller: 'PaymentsReviewCtrl'
      }).
      when('/pending_quotas', {
        templateUrl: 'views/reports/overdue_quotas.html',
        controller: 'OverdueQuotasCtrl'
      }).
      otherwise({
        redirectTo: '/partners'
      });
  }]).
  run(function($rootScope, $location){
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      if(!userLogged($rootScope)) {
        if(next.templateUrl != "views/auth/login.html"){
          $location.path( "/login" );
        };
      };
    });
  });

function userLogged($rootScope){
  return $rootScope.current_user && $rootScope.current_user.logged;
};

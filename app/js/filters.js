var billingFilters = angular.module('billingFilters', []);

billingFilters.filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713 Sí' : '\u2718  No';
  };
})

billingFilters.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

billingFilters.filter("spanishifyMonthNumber", function() {
  return function(monthNumber){
    switch(monthNumber) {
      case 1:
        return 'enero';
      case 2:
        return 'febrero';
      case 3:
        return 'marzo';
      case 4:
        return 'abril';
      case 5:
        return 'mayo';
      case 6:
        return 'junio';
      case 7:
        return 'julio';
      case 8:
        return 'agosto';
      case 9:
        return 'septiembre';
      case 10:
        return 'octubre';
      case 11:
        return 'noviembre';
      case 12:
        return 'diciembre';
      default:
        return monthNumber;
    };
  }
});

billingFilters.filter('paymentMethod', function() {
  return function(input) {
    switch(input){
      case 'cash':
        return 'Efectivo';
      case 'credit':
        return 'Crédito';
      case 'debit':
        return 'Débito';
      case 'check':
        return 'Cheque';
      case 'transfer':
        return 'Transferencia';
      case 'pagomiscuentas':
        return 'Pago mis cuentas';
      case 'bonification':
        return 'Bonificación'; 
      default:
        return input;
    };
  };
})

billingFilters.filter('hasMiniumOverdueQuotasCount', function() {
  return function(partners, overdueQuotasCount) {
    var filtered_partners = [];
    angular.forEach(partners, function(partner) {
      if(overdueQuotasCount <= partner.overdue_quotas_count) {
        filtered_partners.push(partner);
      }
    });
    return filtered_partners;
  };
});

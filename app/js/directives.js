var billingDirectives = angular.module('billingDirectives', []);

billingDirectives.directive('singleMethodPaymentsList', function() {
  return {
    restrict: 'E',
    scope: { paymentReview: '=paymentReview' },
    templateUrl: 'views/payments_reviews/single_method_payments_list.html'
  };
});

billingDirectives.directive('fromUpToInput', function() {
  return {
    restrict: 'E',
    scope: {
      from: '=from',
      upTo: '=upTo',
      fromUpToChanged: '=fromUpToChanged'
    },
    templateUrl: 'views/from_up_to_input.html'
  };
});

billingDirectives.directive('receiptsTable', function() {
  return {
    restrict: 'E',
    scope: { receipts: '=receipts' },
    templateUrl: 'views/receipts/receipts_table.html'
  };
});

var billingServices = angular.module('billingServices', ['ngResource']);
var config = require('./config');
var server = 'http://' + config.billing_server.host + ':' + config.billing_server.port;

function serverUrl(path){
  return server + path;
};

function setPartnersBehaviour(partners){
  partners.prototype.route = function() {
      return "#/partners/" + this.id;
  };

  return partners;  
};

billingServices.factory('Session', ['$resource', function($resource){
  return $resource(serverUrl('/sessions.json'), {}, { 
    create: { method:'POST', isArray:false },
    destroy: { method:'DELETE', isArray:false}
    });
}]);

billingServices.factory('Partner', ['$resource',
  function($resource){
    return setPartnersBehaviour($resource(server + '/partners/:partnerId.json', {}, {
      query: {method:'GET', params:{partnerId:''}, isArray:true},
      create: {method:'POST', params:{partnerId:''}, isArray:false},
      update: {method:'PUT', isArray:false}
    }));
  }]);

billingServices.factory('LoggedUsername', ['$resource', function($resource){
  return $resource(serverUrl('/logged_username.json'), {}, {});
}]);

billingServices.factory('Receipt', ['$resource',
  function($resource){
    var Receipt = $resource(server + '/receipts/:receiptId.json', {}, {
      query: {method:'GET', params:{receiptId:''}, isArray:true},
      create: {method:'POST', params:{receiptId:''}, isArray:false},
      destroy: {method:'DELETE', isArray:false}
    });

    return Receipt;
  }]);

billingServices.factory('PartnerCategory', ['$resource',
  function($resource){
    return $resource(server + '/partner_categories/:partnerCategoryId.json', {}, {
      query: {method:'GET', params:{partnerCategoryId:''}, isArray:true},
      create: {method:'POST', params:{partnerCategoryId:''}, isArray:false},
      update: {method:'PUT', isArray: false}
    });
  }]);

billingServices.factory('PartnerQuotas', ['$resource',
  function($resource){
    return $resource(server + '/partners/:partnerId/quotas/:quotaId.json', {}, {
      query: {method:'GET', params:{partnerId: $resource.partnerId, quotaId: ''}, isArray:true}
    });
  }]);

billingServices.factory('PartnerCategoryPartners', ['$resource',
  function($resource){
    return setPartnersBehaviour($resource(server + '/partner_categories/:partnerCategoryId/partners.json', {}, {
          query: {method:'GET', params:{partnerCategoryId: $resource.partnerCategoryId}, isArray:true}
        }));
  }]);

billingServices.factory('ComingQuota',['$resource',
  function($resource){
    return $resource(server + '/partners/:partnerId/coming_quota.json');
  }]);

billingServices.factory('NextQuota',['$resource',
  function($resource){
    return $resource(server + '/quotas/:quotaId/next.json');
  }]);

billingServices.factory('ClientSeminaryOffer',['$resource',
  function($resource){
    return $resource(server + '/clients/:clientId/seminary_offers.json', {},{
      query: { method: 'GET', params: { clientId: $resource.clientId }, isArray: true }
    }); 
  }]);

billingServices.factory('NextReceiptLegalCode', ['$resource',
  function($resource){
    return $resource(server + '/next_receipt_legal_code.json');
  }]);

billingServices.factory('PaymentMethod', ['$resource',
  function($resource){
    return $resource(server + '/payment_methods.json',{},{
      query: { method: 'GET', params: {}, isArray: true}
    });
  }]);

billingServices.factory('User', ['$resource', 
  function($resource){
    return $resource(serverUrl('/users/:userId.json'),{},{
      query: { method: 'GET', params: {userId: ''}, isArray: true},
      create: { method: 'POST', params: {userId: ''}, isArray: false},
      update: {method:'PUT', isArray:false}
    });
  }]);

billingServices.factory('Seminary', ['$resource',
  function($resource){
    return $resource(serverUrl('/seminaries/:seminaryId.json'), {}, {
      query: { method: 'GET', params: {seminaryId: ''}, isArray: true },
      create: { method: 'POST', params: {seminaryId: ''}, isArray: false},
      update: { method:'PUT', isArray:false }
    });
  }]);

billingServices.factory('SeminaryCategoryPrices', ['$resource', 
  function($resource){
    return $resource(serverUrl('/seminaries/:seminaryId/seminary_category_prices/:categoryPriceId.json'), {}, {
      query: {method:'GET', params: {categoryPriceId: ''}, isArray: true},
      create: {method:'POST', params: {categoryPriceId: ''}, isArray: false}
    });
  }]);

billingServices.factory('SeminaryNotDefinedCategories', ['$resource',
  function($resource){
    return $resource(serverUrl('/seminaries/:seminaryId/not_defined_categories.json'), {}, {
      query: {method:'GET', isArray: true}
    });
  }]);

billingServices.factory('PartnerReceipt', ['$resource',
  function($resource){
    return $resource(serverUrl('/partners/:partnerId/receipts.json'), {}, {
      query: {method:'GET', isArray: true}
    });
  }]);

billingServices.factory('PaymentsReview', ['$resource',
  function($resource){
    return $resource(serverUrl('/receipt_lines.json'), {}, {
      query: {method:'GET', isArray: true}
    });
  }]);

billingServices.factory('ReceiptsReport', ['$resource',
  function($resource){
    return $resource(serverUrl('/receipts_report.json'), {}, {});
  }]);  

billingServices.factory('OverdueQuotasReport', ['$resource',
  function($resource){
    return $resource(serverUrl('/overdue_quotas_report.json'),{},{});
  }]);

billingServices.factory('EachPaymentReview', ['$resource', 
  function($resource){
    return $resource(serverUrl('/receipt_lines/each_payment_review.json'), {}, {});
  }]);
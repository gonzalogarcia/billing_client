var PDFDocument = require('pdfkit');
var fs = require('fs');
var gui = require('nw.gui');

var billingControllers = angular.module('billingControllers', []);

billingControllers.controller('NewUserCtrl', ['$scope', '$location', 'User',
  function($scope, $location, User){
    $scope.user = {};

    $scope.finishUserEdit = createWithNotifications(
      User, 
      {user: $scope.user}, 
      function(value){
        showNotification(
          './icons/user.png', 
          "Nuevo usuario creado", 
          value.username + " fue creado exitosamente.");

        $location.path('/users');
      }, 
      function(error){
        showNotification(
          './icons/error.png', 
          "No se pudo crear al usuario", 
          error.data.error);
      });
  }]);

billingControllers.controller('EditUserCtrl', ['$scope', '$routeParams', '$location', 'User',
  function($scope, $routeParams, $location, User){
    $scope.user = User.get({ userId: $routeParams.userId });
    
    $scope.finishUserEdit = function(){
      User.update({ userId: $scope.user.id }, { user: $scope.user })
        .$promise.then(function(value){
          showNotification(
            './icons/user.png', 
            'Usuario modificado exitosamente', 
            value.username + " fue actualizado.");
          $location.path('/users');
        },function(error){
          showNotification('./icons/error.png', "No se pudo guardar los cambios", error.data.error);
        });
    };
  }]);

billingControllers.controller('EditPartnerCtrl',
  ['$scope', '$routeParams', 'PartnerCategory', 'Partner', '$location', 
  function($scope, $routeParams, PartnerCategory, Partner, $location) {
    $scope.partnerCategories = PartnerCategory.query();

    $scope.partner = Partner.get({partnerId: $routeParams.partnerId});

    $scope.finishPartnerEdit = function(){
      $scope.partner
        .$update({partnerId: $scope.partner.id})
        .then(function(value){
          showNotification(
            './icons/partner.png', 
            "Cliente editado", 
            value.full_name + " fue editado exitosamente.");
          goToPartnerProfile($location, value.id);
        }, 
        function(err){
          showNotification(
            './icons/error.png', 
            "No se pudo guardar los cambios", 
            err.data.error);
        });
    };
}]);

billingControllers.controller('LoginCtrl', ['$scope', '$rootScope', '$location', 'Session', 
  function ($scope, $rootScope, $location, Session){
    $scope.user = {};
    $scope.createSession = function(){
      Session.create($scope.user).$promise.then(
        function(val){
          val.logged = true;
          $rootScope.current_user = val;
          $location.path('/partners');
        },
        function(err){
          showNotification('./icons/error.png', "No se pudo iniciar sesión", err.data.error);
        });
    };
}]);

billingControllers.controller('HeaderCtrl', [ '$scope', '$rootScope', '$location', 'Session',
  function($scope, $rootScope, $location, Session){
    $scope.logout = function(){
      Session.destroy().$promise.then(
      function(val){
        $rootScope.current_user = { logged: false };
        showNotification("./icons/user.png", "Sesión finalizada", "Su sesión fue cerrada");
        $location.path('/login');
      }, 
      function(err){
        showNotification("./icons/error.png", "No se pudo cerrar la sesión", err.data.error);
      });
    };
  }]);

billingControllers.controller('ReceiptListCtrl', ['$scope', 'ReceiptsReport', function ($scope, ReceiptsReport) {
    $scope.receiptsReport = { receipts: [] };
    $scope.from = new Date;
    $scope.up_to = new Date;

    $scope.refreshReport = function(){
      $scope.from.setHours(0, 0, 0);
      $scope.up_to.setHours(23, 59, 59);
      $scope.receiptsReport = ReceiptsReport.get(
        { 
          from: $scope.from,
          up_to: $scope.up_to
        });
    };

    $scope.download = function(){
      receiptsReportDoc($scope.from, $scope.up_to, $scope.receiptsReport).download('Recibos_emitidos.pdf');
    };
}]);

billingControllers.controller('PartnerListCtrl', ['$scope', 'Partner', function ($scope, Partner) {
  $scope.partners = Partner.query();
}]);

billingControllers.controller('PartnerProfileCtrl', 
  ['$scope', '$routeParams', 'Partner', 'PartnerQuotas', 'PartnerReceipt',
  function($scope, $routeParams, Partner, PartnerQuotas, PartnerReceipt) {
    $scope.partner = Partner.get({partnerId: $routeParams.partnerId});
    $scope.quotas = PartnerQuotas.query({partnerId: $routeParams.partnerId});
    $scope.receipts = PartnerReceipt.query({partnerId: $routeParams.partnerId});
}]);

billingControllers.controller('NewPartnerCtrl',
  ['$scope', 'PartnerCategory', 'Partner', '$location', function($scope, PartnerCategory, Partner, $location) {
    $scope.partnerCategories = PartnerCategory.query();

    $scope.partner = {};

    $scope.finishPartnerEdit = createWithNotifications(
      Partner, 
      $scope.partner, 
      function(value){
        showNotification(
          './icons/create_partner.png', 
          "Nuevo cliente registrado", 
          value.first_name + ' ' + value.last_name + " fue agregado exitosamente.");

        goToPartnerProfile($location, value.id);
      }, 
      function(error){
        showNotification(
          './icons/error.png', 
          "No se pudo registrar al nuevo cliente", 
          error.data.error);
      });
}]);


billingControllers.controller('NewPartnerCategoryCtrl',
  ['$scope', 'PartnerCategory', '$location', function($scope, PartnerCategory, $location) {
    $scope.partner_category = {};

    $scope.finishPartnerCategoryEdit = createWithNotifications(
      PartnerCategory, 
      $scope.partner_category, 
      function(value){
        showNotification(
          './icons/create_partner_category.png', 
          "Nueva categoría registrada", 
          value.description + " fue agregada exitosamente.");

        goToPartnerCategoryProfile($location, value.id);
      }, 
      function(error){
        showNotification(
          './icons/error.png', 
          "No se pudo registrar a la nueva categoría", 
          error.data.error);
      });
}]);

billingControllers.controller('EditPartnerCategoryCtrl',
  ['$scope', 'PartnerCategory', '$location', '$routeParams', 
  function($scope, PartnerCategory, $location, $routeParams) {
    $scope.partner_category = PartnerCategory.get({partnerCategoryId: $routeParams.partnerCategoryId});

    $scope.finishPartnerCategoryEdit = function(){
      $scope.partner_category
        .$update({partnerCategoryId: $scope.partner_category.id})
        .then(function(value){
          showNotification(
            './icons/edit_partner_category.png', 
            "Categoría editada", 
            value.description + " fue editada exitosamente.");
          goToPartnerCategoryProfile($location, value.id);
        }, 
        function(err){
          showNotification(
            './icons/error.png', 
            "No se pudo guardar los cambios", 
            error.data.error);
        });
    };
}]);

function goToPartnerProfile($location, partner_id){
  $location.path('/partners/' + partner_id);
}

var NW = require('nw.gui');

// Extend application menu for Mac OS
if (process.platform == "darwin") {
  var menu = new NW.Menu({type: "menubar"});
  menu.createMacBuiltin && menu.createMacBuiltin(window.document.title);
  NW.Window.get().menu = menu;
}

function showNotification(icon, title, body) {
  if (icon && icon.match(/^\./)) {
    icon = icon.replace('.', 'file://' + process.cwd());
  }

  var notification = new Notification(title, {icon: icon, body: body});

  notification.onclose = function () {
    NW.Window.get().focus();
  };

  return notification;
}

billingControllers.controller('PartnerCategoryProfileCtrl', [
  '$scope', 
  '$routeParams', 
  'PartnerCategory',
  'PartnerCategoryPartners',
  function($scope, $routeParams, PartnerCategory, PartnerCategoryPartners){
    $scope.partnerCategory = PartnerCategory.get({partnerCategoryId: $routeParams.partnerCategoryId});
    $scope.partners = PartnerCategoryPartners.query({partnerCategoryId: $routeParams.partnerCategoryId});
  }]);

billingControllers.controller('PartnerCategoryListCtrl', ['$scope', '$routeParams', 'PartnerCategory',
  function($scope, $routeParams, PartnerCategory){
    $scope.partnerCategories = PartnerCategory.query();
  }]);

billingControllers.controller('PartnerNewReceiptCtrl', [
  '$scope', 
  '$routeParams',
  '$location',
  'Partner', 
  'Receipt',
  'ComingQuota',
  'NextQuota',
  'ClientSeminaryOffer',
  'NextReceiptLegalCode',
  'PaymentMethod',
  function($scope, $routeParams, $location, 
    Partner, Receipt, ComingQuota, NextQuota, ClientSeminaryOffer, NextReceiptLegalCode, PaymentMethod){

    $scope.paymentMethods = PaymentMethod.query(); 
    
    $scope.legal_code = NextReceiptLegalCode.get({});

    $scope.receipt = {
      partner: Partner.get({partnerId: $routeParams.partnerId}),
      quota_lines: [],
      seminary_lines: [],
      other_lines: []
    };

    $scope.seminaries = ClientSeminaryOffer.query({clientId: $routeParams.partnerId});

    var newLineFor = function(q){
      return { quota: q, amount: q.pending_amount };
    };

    $scope.newQuotaLine = initializeQuotaLineFor($routeParams.partnerId, ComingQuota);
    $scope.newSeminaryLine = {};
    $scope.newOtherLine = {};

    $scope.createReceipt = function(){
      $scope.receipt.legal_letter = $scope.legal_code.letter;
      $scope.receipt.legal_place_code = $scope.legal_code.place;
      $scope.receipt.legal_instance_code = $scope.legal_code.instance;

      createWithNotifications(
        Receipt, 
        $scope.receipt, 
        function(value){
          showNotification(
            './icons/eol.png', 
            "Nuevo recibo registrado", 
            "El recibo fue agregado exitosamente.");

          goToReceiptProfile($location, value.id);
        }, 
        function(error){
          showNotification(
            './icons/error.png', 
            "No se pudo registrar al recibo", 
            error.data.error);
        })();
    }
    
    $scope.addQuotaLine = function(){
      var paidQuota = $scope.newQuotaLine.quota;
      $scope.newQuotaLine.description = 
        "Cuota " + paidQuota.month + '/' + paidQuota.year + ' ' + paidQuota.partner_category.description;
      paidQuota.pending_amount -= $scope.newQuotaLine.amount;

      $scope.receipt.quota_lines.push($scope.newQuotaLine);

      if(paidQuota.pending_amount > 0){
        $scope.newQuotaLine = { quota: paidQuota, amount: paidQuota.pending_amount };
      }else{
        $scope.newQuotaLine = newLineFor(NextQuota.get({ quotaId: paidQuota.id }));
      };
    };

    $scope.addOtherLine = function(){
      $scope.receipt.other_lines.push($scope.newOtherLine);
      $scope.newOtherLine = {};
    };

    $scope.addSeminaryLine = function(){
      $scope.newSeminaryLine.description = $scope.newSeminaryLine.seminary_offer.description;
      $scope.newSeminaryLine.seminary_id = $scope.newSeminaryLine.seminary_offer.seminary_id;

      $scope.receipt.seminary_lines.push($scope.newSeminaryLine);
      $scope.newSeminaryLine = {};
    };

  }]);

billingControllers.controller('ReceiptProfileCtrl', [
  '$scope', 
  '$routeParams',
  '$location',
  '$filter',
  'Receipt',
  'Partner',
  function($scope, $routeParams, $location, $filter, Receipt, Partner){
    $scope.receipt = Receipt.get({ receiptId: $routeParams.receiptId });

    $scope.print = function(){
      generatePDF($scope.receipt, $filter);
    };

    $scope.cancel = function(){
      $scope.receipt.$destroy({receiptId: $scope.receipt.id}).then(
        function(value){
          showNotification(
            'icons/receipt.png',
            'Recibo cancelado',
            'El recibo del ' + value.date_of_issue_description + ' fue cancelado.');
          $location.path('/receipts');
        }, function(error){
          showNotification(
            'icons/error.png',
            'El recibo no pudo ser cancelado',
            error.data.error);
        });
    };
  }]);

function initializeQuotaLineFor(partnerId, ComingQuota){
  var line = { quota: ComingQuota.get({partnerId: partnerId}) };
  line.amount = line.quota.pending_amount;
  return line;
};

function createWithNotifications(service, instance, successAction, failureAction){
  return function() {
    service.create(instance).$promise.then(successAction,failureAction);
  };
};

function goToReceiptProfile($location, receipt_id){
  $location.path('/receipts/' + receipt_id);
};

function goToPartnerCategoryProfile($location, partner_category_id){
  $location.path('/partner_categories/' + partner_category_id);
};

function generatePDF(receipt, $filter){
  var doc = new PDFDocument({
    size: [637.8, 864.567],
    margins : {
      top: 42.5, 
      bottom: 113.4,
      left: 35,
      right: 35
    }});

  var path = process.cwd() + '/tmp/receipt.pdf';

  //fs.unlinkSync(path);
  doc.pipe(fs.createWriteStream(path));

  doc.fontSize(10);

  doc.text(receipt.date_of_issue_simple_description, {align: 'right'});
  
  doc.moveDown(6)
    .text(receipt.client_full_name)
    .text(receipt.client_address)
    .text(receipt.client_place_with_postal_code)
    .text(receipt.client_province);
  
  doc.moveDown(11);
  
  var addReceiptLine = function(description, price){
    doc.text(description).moveUp().text(price, {align: 'right'});
  };

  addReceiptLine("Descripción", "Precio");
  doc.moveDown(3);
  receipt.receipt_lines.forEach(function(l){
    addReceiptLine(l.description + ' (' + $filter('paymentMethod')(l.payment_method)+ ')', '$' + l.amount); 
  });

  doc.text('TOTAL $' + receipt.full_amount, doc.x, 715, {align: 'right'});

  doc.end();

  showNotification(
    './icons/receipt.png',
    'Recibo listo',
    "Recibo listo para imprimir.");

  gui.Shell.openItem(path);
};

billingControllers.controller('UsersListCtrl', ['$scope', 'User', function ($scope, User) {
  $scope.users = User.query();
  
  $scope.delete = function(user){
    user.$delete({userId: user.id}).then(
      function(value){
        var index = $scope.users.indexOf(user);
        $scope.users.splice(index, 1);
        showNotification(
          './icons/user.png',
          'Usuario eliminado',
          user.username + ' fue eliminado exitosamente.');
      }, function(err){
        showNotification(
          './icons/error.png',
          user.username + ' no fue eliminado',
          err.data.error);
      });
  };
}]);

billingControllers.controller("SeminariesListCtrl", ['$scope', 'Seminary', 
  function($scope, Seminary){
    $scope.seminaries = Seminary.query();
  }]);

billingControllers.controller("SeminaryProfileCtrl", 
  ['$scope', '$routeParams', 'Seminary', 'SeminaryCategoryPrices', 'PartnerCategory',
  function($scope, $routeParams, Seminary, SeminaryCategoryPrices, PartnerCategory){
    $scope.seminary = Seminary.get({seminaryId: $routeParams.seminaryId});
    $scope.categories_prices = SeminaryCategoryPrices.query({seminaryId: $routeParams.seminaryId});

    $scope.categories = PartnerCategory.query();
    $scope.newCategoryPrice = {};
    $scope.addCategoryPrice = function() {
      SeminaryCategoryPrices.create({seminaryId: $routeParams.seminaryId}, 
        { seminary_category_price: $scope.newCategoryPrice }).$promise.then(
        function(value){
          $scope.newCategoryPrice = {};
          $scope.categories = PartnerCategory.query();
          $scope.categories_prices = SeminaryCategoryPrices.query({seminaryId: $routeParams.seminaryId});
          showNotification(
            './icons/seminary.png', 
            "Nuevo precio especificado", 
            "El precio para " + value.partner_category_description + 
            " para " + value.seminary_title + " fue agregado exitosamente.");
        }, function(error){
          showNotification(
            './icons/error.png', 
            "No se pudo especificar el nuevo precio", 
            error.data.error);
        });
    };
  }]);

billingControllers.controller("NewSeminaryCtrl", ['$scope', '$routeParams', '$location', 'Seminary', 
  function($scope, $routeParams, $location, Seminary){
    $scope.seminary = {};
    
    $scope.finishSeminaryEdit = createWithNotifications(
      Seminary, 
      $scope.seminary, 
      function(value){
        showNotification(
          './icons/seminary.png', 
          "Nuevo seminario creado", 
          value.title + " fue creado exitosamente.");

        $location.path('/seminaries/' + value.id);
      }, 
      function(error){
        showNotification(
          './icons/error.png', 
          "No se pudo crear al seminario", 
          error.data.error);
    });
  }]);

billingControllers.controller("EditSeminaryCtrl", ['$scope', '$routeParams', '$location', 'Seminary', 
  function($scope, $routeParams, $location, Seminary){
    $scope.seminary = Seminary.get({seminaryId: $routeParams.seminaryId});
    
    $scope.finishSeminaryEdit = function(){
      $scope.seminary.$update({seminaryId: $scope.seminary.id}).then(
        function(value){
          showNotification(
            './icons/seminary.png', 
            "Seminario editado", 
            value.title + " fue editado exitosamente.");
          $location.path('/seminaries/' + value.id);
        }, function(error){
          showNotification(
            './icons/error.png', 
            "No se pudo editar al seminario", 
            error.data.error);
        });
    }
  }]);

billingControllers.controller("PaymentsReviewCtrl", ['$scope', '$filter', 'EachPaymentReview',
  function($scope, $filter, EachPaymentReview){
    $scope.from = new Date; 
    $scope.up_to = new Date;

    function query(){
      $scope.from.setHours(0, 0, 0); 
      $scope.up_to.setHours(23, 59, 59);

      return EachPaymentReview.get({ from: $scope.from, up_to: $scope.up_to });
    };

    $scope.getReview = function(){
      $scope.each_payment_review = query();
    };

    $scope.getReview();

    $scope.download = function(){
      function paymentsTable(review){
        var table = { 
          table: { 
            headerRows: 1,
            widths: [ 70, 70, 70, 70, 70, 70 ],
            style: 'table_lines'
          },
          style: 'paymentsTable'
        };

        tableRows = review.lines.map(function(l){
          return [
            l.date_of_issue_simple_description, 
            l.receipt_code,
            l.description,
            l.client_full_name,
            l.client_cuit,
            l.amount
          ].map(function(e){
            return { 
              text: (e || "").toString(), 
              style: 'cellData' 
            };
          });
        });

        table.table.body = [
          ['Fecha', 'Recibo', 'Motivo', 'Nombre y apellido', 'CUIT', 'Importe'].map(
            function(t){ 
              return { text: t, style: 'tableHeaders'};
            })
        ].concat(tableRows);

        table.table.body.push(['', '', 'TOTAL', '', '', review.full_amount.toString()]);
        return table;
      };


      var docDefinition = { 
        content: [
          { text: "E.O.L.", style: 'logo' },
          { text: "COBROS POR MEDIO DE PAGO", style: 'title' },
          { text: "Desde fecha: " + formatDate($scope.from), style: 'date_title' },
          { text: "Hasta fecha: " + formatDate($scope.up_to), style: 'date_title' }
        ],
        styles: {
          title: {
            fontSize: 14,
            bold: true
          },
          table_lines: {
            italic: true,
            alignment: 'right'
          },
          paymentsTable: {
            margin: [0, 0, 0, 0]
          },
          payment_title: {
            underlined: true,
            color: 'green',
            bold: true,
            margin: [0, 25, 0, 0]
          },
          logo: {
            fontSize: 15,
            bold: true,
            borderStyle: 'solid',
            borderWidth: '5px',
          },
          cellData: {
            fontSize: 8
          },
          tableHeaders: {
            bold: true,
            underline: true,
            fontSize: 8
          }
        }
      };

      $scope.each_payment_review.reviews.forEach(function(review){
        docDefinition.content.push({ text: $filter('paymentMethod')(review.payment_method_description), style: 'payment_title' });
        docDefinition.content.push(paymentsTable(review));
      });

      docDefinition.content.push({ text: 'Total: ' + $scope.each_payment_review.full_amount });
      docDefinition.content.push({ text: 'Total cuotas: ' + $scope.each_payment_review.quota_amount });
      docDefinition.content.push({ text: 'Total otros motivos: ' + $scope.each_payment_review.others_amount });
      docDefinition.content.push({ text: 'Total abonado por categoría:' });
      docDefinition.content.push({ 
        table: { 
          widths: [350, 70],
          body: $scope.each_payment_review.partner_category_reviews.map(function(cr){
            return [{ text: cr.partner_category.description }, { text: cr.full_amount.toString() }];
          })
        }
      });

      pdfMake
        .createPdf(docDefinition)
        .download('Cobros_' + formatDate($scope.from) + '_' + formatDate($scope.up_to) + '.pdf');
    };
  }]);

function receiptsReportDoc(from, up_to, report){
  function receiptsTable(report){
    var table = { 
      table: { 
        headerRows: 1,
        widths: [ 70, 70, 130, 70, 70],
        style: 'table_lines'
      },
      style: 'receiptsTable'
    };

    tableRows = report.receipts.map(function(r){
      return [
        { text: (r.date_of_issue_simple_description || '').toString(), style: 'cellData' }, 
        { text: (r.legal_code_description || '').toString(), style: 'cellData' },
        { text: (r.client_full_name || '').toString(), style: 'cellData' },
        { text: (r.client_cuit || '').toString(), style: 'cellData' },
        { text: (r.full_amount || '').toString(), style: ['cellData', 'pullRight'] }
      ];
    });

    tableRows.push([
      {text: 'TOTAL', style: ['cellData'], bold: true },
      '', '', '',
      {text: (report.amount || '').toString(), style: ['cellData', 'pullRight'], bold: true }]);

    table.table.body = [
      ['Fecha', 'Recibo', 'Nombre y apellido', 'CUIT', 'Importe'].map(
        function(t){ 
          return { text: t, style: 'tableHeaders'};
        })
    ].concat(tableRows);

    return table;
  };
  
  var docDefinition = { 
        content: [
          { text: "E.O.L.", style: 'logo' },
          { text: "SUBDIARIO DE RECIBOS EMITIDOS", style: 'title' },
          { text: "Desde fecha: " + formatDate(from), style: 'date_title' },
          { text: "Hasta fecha: " + formatDate(up_to), style: 'date_title' },
          receiptsTable(report)
        ],
        styles: {
          title: {
            fontSize: 14,
            bold: true
          },
          table_lines: {
            italic: true,
            alignment: 'right'
          },
          paymentsTable: {
            margin: [0, 0, 0, 0]
          },
          payment_title: {
            underlined: true,
            color: 'green',
            bold: true,
            margin: [0, 25, 0, 0]
          },
          logo: {
            fontSize: 15,
            bold: true,
            borderStyle: 'solid',
            borderWidth: '5px',
          },
          cellData: {
            fontSize: 8
          },
          pullRight: {
            alignment: 'right'
          },
          tableHeaders: {
            bold: true,
            underline: true,
            fontSize: 8
          }
        }
      };
  return pdfMake.createPdf(docDefinition);
};

function formatDate(d){ 
  return d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear(); 
};

billingControllers.controller("OverdueQuotasCtrl", ['$scope', 'OverdueQuotasReport',
  function($scope, OverdueQuotasReport){
    $scope.overdue_quotas_minium = 0;
    
    $scope.refresh = function(){
      $scope.report = OverdueQuotasReport.get({ minium_quotas_count: $scope.overdue_quotas_minium });
    };

    $scope.refresh();

    $scope.download = function(){
      overdueQuotasReportDoc($scope.report).download("Cuotas_atrasadas.pdf");
    };
  }]);

function overdueQuotasReportDoc(report){
  var docDefinition = { 
        content: [
          { text: "E.O.L.", style: 'logo' },
          { text: "Adhesiones pendientes al " + formatDate(new Date), style: 'title' },
          { text: "Con mínimo de " + report.minium_quotas_count + " cuotas atrasadas."}
        ],
        styles: {
          title: {
            fontSize: 14,
            bold: true
          },
          table_lines: {
            italic: true,
            alignment: 'right'
          },
          logo: {
            fontSize: 15,
            bold: true,
            borderStyle: 'solid',
            borderWidth: '5px',
          },
          cellData: {
            fontSize: 8
          },
          pullRight: {
            alignment: 'right'
          },
          tableHeaders: {
            bold: true,
            underline: true,
            fontSize: 8
          },
          categoryTitle: {
            margin: [0, 30, 0, 0]
          }
        }
      };

  report.categories_overdue_quotas.forEach(function(c){
    docDefinition.content.push({ text: c.category_description, style: 'categoryTitle' }); 
    
    var table = { 
      table: { 
        headerRows: 1,
        widths: [ 300, 70, 120],
        style: 'table_lines'
      }
    };

    table.table.body = [['Apellido y nombre', 'Cuotas','Importe']]

    c.partners.forEach(function(p){
      table.table.body.push(
        [{ text: (p.full_name || '').toString(), style: 'cellData' }, 
        { text: (p.overdue_quotas_count || '0').toString(), style: ['pullRight', 'cellData'] }, 
        { text: (p.overdue_quotas_amount || '0').toString(), style: ['pullRight', 'cellData'] }]);      
    });

    table.table.body.push(
        [{ text: 'Total ' + c.category_description, style: ['cellData'], bold: true }, 
          { text: (c.count || '0').toString(), style: ['pullRight', 'cellData'], bold: true  }, 
          { text: (c.amount || '0').toString(), style: ['pullRight', 'cellData'], bold: true  }]);

    docDefinition.content.push(table);
  });

  docDefinition.content.push({ 
    table: {
      headerRows: 0,
      widths: [ 300, 70, 120],
      style: 'table_lines',
      body: [['Total general', report.count, report.amount].map(function(e){
                return { text: (e || "0").toString(), style: ['pullRight', 'cellData'] };
              })]
    },
    margin: [0, 50, 0, 0]
  });

  return pdfMake.createPdf(docDefinition);
};

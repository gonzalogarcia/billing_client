# Billing Client

## Overview

This application is a very friendly Billing client. The easy way to have track of the quota, seminaries, and other payments of your association. It works on Linux, Windows and OSX.

## Settings

This node app is supposed to be run with Node Webkit. Just as with any Node Webkit app, run the nw executable with the path to this project and voilà!

In order to be able to connect to the server, just add a line to the hosts file from your OS (e.g. '/etc/hosts' on most Linux systems) pointing the server IP with the translation billingserver.